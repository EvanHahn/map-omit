import assert from "assert";
import mapOmit from "../index";

const map = new Map([
  ["foo", 1],
  ["bar", 2],
  ["baz", 3],
]);

// Omitting nothing
assert.strictEqual(mapOmit(map, []).size, 3);

// Omitting a couple of keys
assert.deepStrictEqual(mapOmit(map, ["foo", "bar"]), new Map([["baz", 3]]));

// Omitting missing keys
assert.deepStrictEqual(
  mapOmit(map, ["qux"]),
  new Map([
    ["foo", 1],
    ["bar", 2],
    ["baz", 3],
  ])
);

// Using a non-array iterable
assert.deepStrictEqual(
  mapOmit(map, new Set(["foo", "bar"])),
  new Map([["baz", 3]])
);

// Doesn't mutate the original
assert.deepStrictEqual(
  map,
  new Map([
    ["foo", 1],
    ["bar", 2],
    ["baz", 3],
  ])
);
