export default function mapOmit<K, V>(
  map: Readonly<Map<K, V>>,
  keys: Iterable<K>
): Map<K, V> {
  const result = new Map(map);
  for (const key of keys) {
    result.delete(key);
  }
  return result;
}
