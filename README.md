# map-omit

Remove keys [`Map`s](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map). Similar to [Lodash's `omit`](https://lodash.com/docs/#omit), but for `Map`s.

```js
import mapOmit from "map-omit";

const myMap = new Map([
  ["foo", 1],
  ["bar", 2],
  ["baz", 3],
]);

const omitted = mapOmit(myMap, ["bar", "baz"]);

omitted.has("bar");
// => false
```
